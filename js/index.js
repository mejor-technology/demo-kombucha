$(function () {
  // Variable ==============================
  const $carouselContainer = $('#js-carousel');

  // Initialization ===========================
  // Carousel Init
  $carouselContainer.slick({
    autoplay: true,
    arrows: false,
    dots: true,
    speed: 1000,
    autoplaySpeed: 3000,
  });

  // Wow Init
  const wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true, // default
  });
  wow.init();
});
