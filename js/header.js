// Variables =================================
const $header = $('#header');
const $mMenuMainLink = $('.m_main_link');
const $ulSub = $('.ul_sub');
const $closeBtn = $('#js_close_nav_btn');
const $openBtn = $('.m_open_menu_btn');
const $mMenu = $('#js_m_menu');
const $mAbsoluteNav = $('#absolute_m_nav');
const $wAbsoluteNav = $('#absolute_w_nav');
const $wMenuAbsoluteMainLink = $('.w_main_absolute_link');
const $wMenuFixedMainLink = $('.w_main_fixed_link');
const $wAbsoluteSubContent = $('#js-absolute-sub-nav-content');
const $wFixedSubContent = $('#js-fixed-sub-nav-content');

let openNavType = '';

// Events =================================

// 子選單開關控制
(function () {
  // Mobile
  $mMenuMainLink.bind('click', function () {
    const hasList = $(this).hasClass('has_ul_sub');
    const isOpen = $(this).hasClass('open');

    if (hasList) {
      $ulSub.slideUp();

      if (!isOpen) {
        $(this).siblings('.ul_sub').slideDown();
        $(this).addClass('open');
      } else {
        $(this).removeClass('open');
      }
    }
  });

  // Web(Absolute)
  $wMenuAbsoluteMainLink.bind('click', function () {
    function countCalculate() {
      let count = 0;

      $wMenuAbsoluteMainLink.each(function () {
        if ($(this).hasClass('open')) count++;
      });

      return count;
    }

    const hasList = $(this).hasClass('has_ul_sub');
    const isOpen = $(this).hasClass('open');
    const target = $(this).data('target');
    const isContainerShow =
      $wAbsoluteSubContent.attr('style').indexOf('block') !== -1;
    const countNum = countCalculate();

    openNavType = 'absolute';

    if (hasList) {
      $('.sub-nav-list').addClass('hidden');
      $(target).removeClass('hidden');
      $wAbsoluteSubContent.slideDown();

      if (isContainerShow) {
        // 外層容器是開的

        if (isOpen) {
          if (countNum === 1) {
            $wAbsoluteSubContent.slideUp();
            $(this).removeClass('open');

            // 背景要變回透明色，關閉遮罩，不能滑動
            $header.removeClass('bg-white');
            scrollYCtrl(true);
            showMask(false);
            openNavType = '';
          }
        } else {
          // 再打開的狀態點了另一個有子項目的分類，關閉前一個並且開啟這一個
          $('.sub-nav-list').addClass('hidden');
          $(target).removeClass('hidden');
          $wMenuAbsoluteMainLink.removeClass('open');
          $(this).addClass('open');
        }
      } else {
        $('.sub-nav-list').addClass('hidden');
        $(target).removeClass('hidden');
        $wAbsoluteSubContent.slideDown();
        $(this).addClass('open');
        // 背景要變白色，有遮罩，不能滑動
        $header.addClass('bg-white');
        scrollYCtrl(false);
        showMask(true);
      }
    }
  });

  // Web(Fixed)
  $wMenuFixedMainLink.bind('click', function () {
    function countCalculateFixed() {
      let count = 0;

      $wMenuFixedMainLink.each(function () {
        if ($(this).hasClass('open')) count++;
      });

      return count;
    }

    const hasList = $(this).hasClass('has_ul_sub');
    const isOpen = $(this).hasClass('open');
    const target = $(this).data('target');
    const isContainerShow =
      $wFixedSubContent.attr('style').indexOf('block') !== -1;
    const countNum = countCalculateFixed();

    openNavType = 'fixed';

    if (hasList) {
      $('.sub-nav-list').addClass('hidden');
      $(target).removeClass('hidden');
      $wFixedSubContent.slideDown();

      if (isContainerShow) {
        // 外層容器是開的

        if (isOpen) {
          if (countNum === 1) {
            $wFixedSubContent.slideUp();
            $(this).removeClass('open');

            // 背景要變回透明色，關閉遮罩，不能滑動
            $header.removeClass('bg-white');
            scrollYCtrl(true);
            showMask(false);
            openNavType = '';
          }
        } else {
          // 再打開的狀態點了另一個有子項目的分類，關閉前一個並且開啟這一個
          $('.sub-nav-list').addClass('hidden');
          $(target).removeClass('hidden');
          $wMenuFixedMainLink.removeClass('open');
          $(this).addClass('open');
        }
      } else {
        $('.sub-nav-list').addClass('hidden');
        $(target).removeClass('hidden');
        $wFixedSubContent.slideDown();
        $(this).addClass('open');
        // 背景要變白色，有遮罩，不能滑動
        $header.addClass('bg-white');
        scrollYCtrl(false);
        showMask(true);
      }
    }
  });

  // 點選 mask 關閉 nav 選單
  $('#page-mask').bind('click', function () {
    if (openNavType === 'absolute') {
      $wAbsoluteSubContent.slideUp();
    } else if (openNavType === 'fixed') {
      $wFixedSubContent.slideUp();
    }

    $('.open').removeClass('open');

    // 背景要變回透明色，關閉遮罩，不能滑動
    $header.removeClass('bg-white');
    scrollYCtrl(true);
    showMask(false);

    openNavType = '';
  });
})();

// 開啟 Menu
$openBtn.bind('click', function () {
  $mMenu.removeClass('hidden');
  $('body').addClass('overflow-hidden');
});

// 關閉 Menu
$closeBtn.bind('click', function () {
  $mMenu.addClass('hidden');
  $('body').removeClass('overflow-hidden');
});
