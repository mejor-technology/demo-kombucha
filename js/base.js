// functions =================================================================
/**
 * @author odin
 * @param {Boolean} isScroll -- true 可以滾動 | false 不能滾動
 * @description 控制頁面能不能往下滾動
 */
function scrollYCtrl(isScroll) {
  if (isScroll) {
    $('body').removeClass('overflow-hidden');
  } else {
    $('body').addClass('overflow-hidden');
  }
}

/**
 * @author odin
 * @param {Boolean} isShow -- true 顯示 | false 隱藏
 * @description 控制Mask要不要顯示
 */
function showMask(isShow) {
  if (isShow) {
    $('#page-mask').fadeIn();
  } else {
    $('#page-mask').fadeOut();
  }
}

/**
 * @author odin
 * @param {string} id -- 要做文字特效的容器的id
 * @description 根據容器中的文字進行文字特效
 */
function textAnimation(id) {
  let text = document.getElementById(id);
  let newDom = '';
  const animationDelay = 20;

  for (let i = 0; i < text.innerText.length; i++) {
    newDom +=
      '<span class="char">' +
      (text.innerText[i] == ' ' ? '&nbsp;' : text.innerText[i]) +
      '</span>';
  }

  text.innerHTML = newDom;
  const length = text.children.length;

  for (let i = 0; i < length; i++) {
    text.children[i].style['animation-delay'] = animationDelay * i + 'ms';
  }
}

// Variables =================================================
const $mFixedeNav = $('#fixed_m_nav');
const $wFixedeNav = $('#fixed_w_nav');
// const $bone = $('#ingredient_bone');
const $wIndexIngreTitleDom = $('#ingredient_w_title');
let count = 0;

// initialize =================================================
(function () {})();

// scroll =================================================
(function () {
  $(window).scroll(function () {
    // 固定的 nav 顯示或是隱藏
    // 手機版
    const TAHeight = $mAbsoluteNav.height();
    if ($(this).scrollTop() >= TAHeight + 50) {
      $mFixedeNav.fadeIn().addClass('flex');
      // alert('header just passed.');
      // instead of alert you can use to show your ad
      // something like $('#footAd').slideup();
    } else {
      $mFixedeNav.fadeOut().removeClass('flex');
    }

    // 桌機版
    const wTAHeight = $wAbsoluteNav.height();
    if ($(this).scrollTop() >= wTAHeight + 50) {
      $wFixedeNav.fadeIn().addClass('flex');
    } else {
      $wFixedeNav.fadeOut().removeClass('flex');
    }

    // 滾動到線的高度
    // const boneH = $bone.height();
    // if ($(window).scrollTop() >= boneH && !$bone.hasClass('bone_hieght')) {
    //   $bone.addClass('bone_hieght');
    // }

    // // 滾動到文字的高度
    const indexIngreTtitleH = $wIndexIngreTitleDom.height();

    if ($(window).scrollTop() >= indexIngreTtitleH + 200) {
      count === 0 ? textAnimation('ingredient_w_title') : null;

      count++;
    }
  });
})();

// resizing =================================================
(function () {
  $(window).resize(function () {});
})();
